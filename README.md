Demonstrate calculation of dribbling rate (dribbles per second) and pace (seconds per dribble) from audio of dribbling.
  
Build:
docker build -t dribbling:dockerfile .
  
Run:
docker run -d -p 5000:5000 dribbling:dockerfile

Post to localhost:5000



