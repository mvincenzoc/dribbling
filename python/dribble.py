import sys
import traceback
import json 
import warnings
warnings.simplefilter(action='ignore', category=FutureWarning)
import numpy as np
import librosa
from scipy.stats import norm

def rolling_window(a, window):
    shape = a.shape[:-1] + (a.shape[-1] - window + 1, window)
    strides = a.strides + (a.strides[-1],)
    return np.lib.stride_tricks.as_strided(a, shape=shape, strides=strides)
    
def rolling_max(x, window):
    return np.max(rolling_window(x, window), -1)

def rolling_nb(x, w=500, b=1, m0 = None, mL = None, mR = 0.25, s0=0.05, sL=0.05, sR=0.05):
    """
    Rolling naive Bayes
    :param w: Window size
    :param b: Short window size
    :param m0: Mean for null distribution.
    :param mL: Mean for left distribution.
    :param mR: Mean for right distribution.
    :param s0: Stddev for null distribution.
    :param sL: Stddev for left distribution.
    :param sR: Stddev for right distribtion.
    :return: Prob of (L,R) vs H0.
    """
    y = np.zeros(x.shape) 
    if m0 is None:
        m0 = np.mean(x)
    if mL is None:
        mL = np.mean(x)
    p0 = np.cumsum(np.log(norm.pdf(x, loc=m0, scale = s0)))
    pL = np.cumsum(np.log(norm.pdf(x, loc=mL, scale = sL)))
    pR = np.cumsum(np.log(norm.pdf(x, loc=mR, scale = sR)))
    l0 = np.zeros(x.shape)
    lL = np.zeros(x.shape)
    lR = np.zeros(x.shape)
    for i in range(w, len(x)):
        l0 = (p0[i] - p0[(i-w)])
        lL = (pL[i-b] - pL[i-w])
        lR = (pR[i] - pR[i-b])
        l1 = lL + lR
        mx = max(l0, l1)
        y[i] = np.exp(l1-mx) / (np.exp(l1-mx) + np.exp(l0-mx))
    return y
    
def dribble_detector(x, sr):
    """
    Dribble detector.
    :param x: Audio samples.
    :param sr: Sample rate.
    """ 
    y = np.abs(x)    
    w = int(sr / 25)
    z = y - np.concatenate((np.zeros(w), rolling_max(y, w)))[:len(y)]
    v = np.where(z > 0, z, 0)
    p = rolling_nb(v, w=500, b=1, m0=None, mL=0.0, mR=np.mean(v[v > 0]))
    t = np.where(p > 0.8, 1, 0)
    ww = np.where(t > 0)[0]
    zz = np.diff(ww)
    uu = np.concatenate(([0], zz))
    vv = ww[uu > 2000]
    bb = vv / sr
    ds = (sr/np.mean(np.diff(vv)))
    sd = 1.0/ds
    return {"status": "ok", "rate": ds, "pace": sd, "count": len(vv), "dribbles": list(bb)}

def dribble_metrics(mp3):
    try:
        # Load the mp3
        x, sr = librosa.load(mp3)
        # Get the dribbles
        dribbles = dribble_detector(x, sr)
        msg = json.dumps(dribbles)
    except:
        traceback.print_exc(file=sys.stdout)
        msg = json.dumps({"status": "error", "trace": traceback.format_exc()})

    # Return result
    return msg

if __name__ == "__main__":
    metrics = dribble_metrics("../audio/dribbling_cross_behind.mp3")
    print(json.loads(metrics)['rate'])
    print(json.loads(metrics)['count'])
