import os
import traceback
from flask import Flask, request, redirect, url_for, send_from_directory
from werkzeug import secure_filename
from dribble import dribble_metrics

app = Flask(__name__)
 
@app.route('/', methods=['GET', 'POST'])
def upload_file():
    if request.method == 'POST':
        msg = "Error"
        try:
            file = request.files['file']
            if file and file.filename[-3:].lower() in ['mp3', 'm4a']:
                filename = secure_filename(file.filename)
                try:
                    file.save(filename)
                    msg = dribble_metrics(filename)
                finally:
                    os.remove(filename)
        except:
            msg = traceback.format_exc()
    else:
        msg = '''
        <!doctype html>
        <title>Upload new File</title>
        <h1>Upload new File</h1>
        <form method=post enctype=multipart/form-data>
        <input type=file name=file>
        <input type=submit value=Upload>
        </form>
        '''
    return msg
            
if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0')
