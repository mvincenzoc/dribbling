FROM ubuntu:latest

RUN apt-get -y update

RUN apt-get install -y python3
RUN apt-get install -y python3-pip
RUN apt-get install -y ffmpeg x264 x265

RUN pip3 install numpy
RUN pip3 install pandas
RUN pip3 install scipy
RUN pip3 install librosa
RUN pip3 install flask
RUN pip3 install werkzeug

RUN groupadd -g 999 appuser && useradd -r -u 999 -g appuser appuser
RUN mkdir /home/appuser
RUN chown -R appuser:appuser /home/appuser
RUN usermod -aG sudo appuser

USER appuser

RUN mkdir /home/appuser/audio
ADD ./audio/* /home/appuser/audio/
RUN mkdir /home/appuser/python
ADD ./python/*.py /home/appuser/python/

WORKDIR /home/appuser/python
CMD python3 app.py 
